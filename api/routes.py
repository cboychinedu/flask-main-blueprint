#!/usr/bin/env python3 

# Importing the necessary modules 
from flask import Blueprint 
from flask import render_template, redirect, url_for 

# Creating the blueprint object for the api 
api = Blueprint('api', __name__) 

# 
@api.route("/", methods=["GET", "POST"])
def DisplayNotifications():
    return "Notifications Home"

# Creating the route for getting notifications 
@api.route("/get_notifications", methods=["GET", "POST"])
def GetNotifications():
    return "Get Notifications"; 


# Creating a route for saving notifications 
@api.route("/save_notifications", methods=["GET", "POST"])
def SaveNotificaions(): 
    return "Save Notifications"; 


# Creating a route to delete notificaions 
@api.route("/delete_notifications", methods=["GET", "POST"])
def DeleteNotifications():
    return "Delete Notifications"; 


# Creating a route for updating notifications 
@api.route("update_notifications", methods=["GET", "POST"])
def UpdateNotifications():
    return "Update Notificaions"; 