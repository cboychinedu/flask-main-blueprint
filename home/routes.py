#!/usr/bin/env/ python3 

# Importing the necessary modules 
from flask import Blueprint
from flask import render_template, redirect, url_for

# Creating the blueprint object 
home = Blueprint('home', __name__)

# Creating the home page  
@home.route('/', methods=["GET"])
def HomePage():
    return "Home Page"; 

# Creating the sign in route 
@home.route('/signin', methods=["GET"])
def SignIn():
    return "Sign In"; 

# Creating the sign up route 
@home.route("/signup", methods=["GET"])
def SignUp():
    return "Sign Up"; 

# Creating the sign out route 
@home.route("/signout", methods=["GET"])
def SignOut():
    return "Sign Out"; 


