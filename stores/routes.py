#!/usr/bin/env python3 

# importing the necessary modules 
from flask import Blueprint 
from flask import render_template, redirect, url_for 

# Creating the blueprint object 
stores = Blueprint('stores', __name__) 

# Creating the route for the stores 
@stores.route('/', methods=["GET", "POST"])
def GetStores():
    return {
        "stores": [ "Sarah", "Mike", "Donald"], 
        "Price": ["$13.45", "$40.56", "$32.345"]
    }; 


# Creating a route for saving the stores 
@stores.route("/save_stores", methods=["GET", "POST"])
def SaveStores():
    return "Save Stores"; 


# Creating a route for updating the stores 
@stores.route("/update_stores", methods=["GET", "POST"])
def UpdateStores():
    return "Update Stores"; 